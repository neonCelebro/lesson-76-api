const express = require('express');
const router = express.Router();

const createRouter = (db) => {
  router.get('/', (req, res) => {
    res.send(db.getData());
  });

  router.post('/', (req, res) => {
    const message = req.body;
    if (message.author.length > 0 && message.message.length > 0){
    db.addMessage(message).then(result => {
      res.send(result);
    });
  }else {
      res.status(400).send({error: "Author and message must be present in the request"})
    }
  });

  router.get('/:date', (req, res) => {
    db.getDataByDate(req.params.date)? res.send(db.getDataByDate(req.params.date))
    : res.status(400).send({error : 'Дата не верна'});
  });

  return router;
};

module.exports = createRouter;